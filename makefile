cppsrc = $(wildcard DainaCompiler/*.cpp) \
	        $(wildcard DainaCompiler/Daina/*.cpp) \
		        $(wildcard DainaCompiler/Daina/Scanner/*.cpp) \
		        $(wildcard DainaCompiler/Daina/Token/*.cpp) \
		        $(wildcard DainaCompiler/Daina/AST/*.cpp) \
			$(wildcard DainaCompiler/Daina/Zither/*.cpp)
obj = $(cppsrc:.cpp=.o)


INCLUDE_FLAGS = -std=c++11  -I./DainaCompiler -I./DainaCompiler/Daina -I./DainaCompiler/Daina/Token -I./DainaCompiler/Daina/AST -I./DainaCompiler/Daina/Scanner -I./DainaCompiler/Daina/Zither


LDFLAGS = -pthread

CXXFLAGS = $(INCLUDE_FLAGS)

daina: $(obj)
	    $(CXX) -o $@ $^ $(LDFLAGS)


clean: 
	rm -f $(obj) daina

